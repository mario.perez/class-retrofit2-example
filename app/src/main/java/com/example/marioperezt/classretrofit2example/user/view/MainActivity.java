package com.example.marioperezt.classretrofit2example.user.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.marioperezt.classretrofit2example.R;
import com.example.marioperezt.classretrofit2example.user.model.DataMessageResponse;
import com.example.marioperezt.classretrofit2example.user.presenter.BreedPresenter;
import com.example.marioperezt.classretrofit2example.user.presenter.IBreedPresenter;

public class MainActivity extends AppCompatActivity implements MessageActivity{

    private TextView tvData;
    private Button btLoad;
    private IBreedPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenter = new BreedPresenter(this);
        tvData = (TextView) findViewById(R.id.tv_text);
        btLoad = (Button) findViewById(R.id.bt_load);
        btLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.getAllBreeds();
            }
        });

    }

    @Override
    public void setData(DataMessageResponse data) {
        String result  = "Respuesta:\n";
        result = result + "TERRIER:"+"\n";
        for(String name:data.getmMessage().getmMessageTerrier()){
            result = result + name+"\n";
        }
        result = result + "SPANIEL:"+"\n";
        for(String name:data.getmMessage().getmMessageSpaniel()){
            result = result + name+"\n";
        }
        result = result + "HOUND:"+"\n";
        for(String name:data.getmMessage().getmMessageHound()){
            result = result + name+"\n";
        }
        tvData.setText(result);
    }
}
