package com.example.marioperezt.classretrofit2example.user.view;

import com.example.marioperezt.classretrofit2example.user.model.DataMessageResponse;

/**
 * Created by marioperezt on 11/24/17.
 */

public interface MessageActivity {
    void setData(DataMessageResponse data);
}
