package com.example.marioperezt.classretrofit2example.dogApi;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by marioperezt on 11/24/17.
 */

public class RestApiAdapter {

    public Service getClientService(){
        OkHttpClient client = getInterceptor();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(Service.class);
    }

    private OkHttpClient getInterceptor() {
        return new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Api-Token","")
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
    }

}
