package com.example.marioperezt.classretrofit2example.dogApi;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by marioperezt on 11/24/17.
 */

public interface Service {
    @GET(Constants.GET_BREEDS)
    Call<JsonObject> getBreeds();

}
