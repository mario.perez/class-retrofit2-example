package com.example.marioperezt.classretrofit2example.user.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by marioperezt on 11/24/17.
 */

public class DataMessageResponse {

    @SerializedName("message")
    private MessageResponse mMessage;

    public MessageResponse getmMessage() {
        return mMessage;
    }

    public void setmMessage(MessageResponse mMessage) {
        this.mMessage = mMessage;
    }
}
