package com.example.marioperezt.classretrofit2example.user.presenter;

import com.example.marioperezt.classretrofit2example.dogApi.RestApiAdapter;
import com.example.marioperezt.classretrofit2example.user.model.DataMessageResponse;
import com.example.marioperezt.classretrofit2example.user.view.MessageActivity;
import com.example.marioperezt.classretrofit2example.utils.GSonUtils;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.zip.DataFormatException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by marioperezt on 11/24/17.
 */

public class BreedPresenter implements IBreedPresenter {

    private MessageActivity mView;
    public BreedPresenter(MessageActivity view){
            mView = view;
    }

    @Override
    public void getAllBreeds() {
        RestApiAdapter api = new RestApiAdapter();
        Call<JsonObject> call = api.getClientService().getBreeds();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                DataMessageResponse result = new GsonBuilder().create()
                        .fromJson(response.body().toString(),DataMessageResponse.class);
                mView.setData(result);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }
}
