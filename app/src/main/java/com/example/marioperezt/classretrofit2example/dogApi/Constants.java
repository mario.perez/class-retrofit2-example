package com.example.marioperezt.classretrofit2example.dogApi;

/**
 * Created by marioperezt on 11/24/17.
 */

public class Constants {
    public static final String ROOT_URL = "https://dog.ceo/api/";
    public static final String GET_BREEDS = "breeds/list/all";
}
