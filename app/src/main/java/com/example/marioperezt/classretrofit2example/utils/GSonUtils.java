package com.example.marioperezt.classretrofit2example.utils;

import com.google.gson.Gson;

/**
 * Created by marioperezt on 11/24/17.
 */

public class GSonUtils<T> {

    public String serializeToJson(T myClass) {
        Gson gson = new Gson();
        String j = gson.toJson(myClass);
        return j;
    }
    public T deserializeFromJson(String jsonString, Class<T> tClass) {
        Gson gson = new Gson();
        T myClass = gson.fromJson(jsonString, tClass);
        return myClass;
    }
}
