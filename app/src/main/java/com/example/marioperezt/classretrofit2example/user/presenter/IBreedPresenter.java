package com.example.marioperezt.classretrofit2example.user.presenter;

/**
 * Created by marioperezt on 11/24/17.
 */

public interface IBreedPresenter {

    void getAllBreeds();

}
