package com.example.marioperezt.classretrofit2example.user.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by marioperezt on 11/24/17.
 */

public class MessageResponse {

    @SerializedName("terrier")
    private List<String> mMessageTerrier;


    @SerializedName("spaniel")
    private List<String> mMessageSpaniel;


    @SerializedName("hound")
    private List<String> mMessageHound;

    public List<String> getmMessageTerrier() {
        return mMessageTerrier;
    }

    public void setmMessageTerrier(List<String> mMessageTerrier) {
        this.mMessageTerrier = mMessageTerrier;
    }

    public List<String> getmMessageSpaniel() {
        return mMessageSpaniel;
    }

    public void setmMessageSpaniel(List<String> mMessageSpaniel) {
        this.mMessageSpaniel = mMessageSpaniel;
    }

    public List<String> getmMessageHound() {
        return mMessageHound;
    }

    public void setmMessageHound(List<String> mMessageHound) {
        this.mMessageHound = mMessageHound;
    }
}
